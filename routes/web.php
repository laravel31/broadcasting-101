<?php

use App\Events\OrderStatusUpdated;
use Illuminate\Support\Facades\Route;


class Order 
{
    public $id;

    public function __construct($id)
    {
        $this->id = $id;
    }
}


Route::get('/', function () {
    return view('welcome');
});

Route::get('update', function() {

    OrderStatusUpdated::dispatch(new Order(33));
});
